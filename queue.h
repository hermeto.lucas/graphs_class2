#ifndef QUEUE_H
#define QUEUE_H

// Fila
typedef struct node {
	int n;
	struct node *next;
} t_node;

typedef t_node *t_pointer;
typedef t_pointer t_queue;

void create(t_queue *queue);
void push(t_queue *queue, int n);
int pop(t_queue *queue);

#endif
