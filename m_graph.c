#include <stdio.h>
#include <stdlib.h>
#include "m_graph.h"
#include "queue.h"

/* Cria Grafo
  Parametros:
    - m_graph *graph: endereço do grafo
    - int n: total de vertices
*/
void m_create(m_graph *graph, int n) {
  graph->n = n;

  graph->matrix = (int **) malloc(n * sizeof(int *));
  int i;
  for (i = 0; i < n; i++) {
    graph->matrix[i] = (int *) malloc(n * sizeof(int));

    int j;
    for (j = 0; j < n; j++)
      graph->matrix[i][j] = -1;
  }
}

/* Imprime Grafo da maneira que foi inserido
  Parametros:
    - m_graph *graph: endereço do grafo
    - m_node node: no' que sera inserido no grafo
*/
void m_printGraph(m_graph *graph) {
  int i;
  for (i = 0; i < graph->n; i++) {
    int j;
    for (j = 0; j < graph->n; j++)
      if (graph->matrix[i][j] == -1)
        printf(". ");
      else
        printf("%d ", graph->matrix[i][j]);

    printf("\n");
  }
}

/* Imprime vertices adjacentes ao vertice x
  Parametros:
    - m_graph *graph: endereço do grafo
    - int x: vertice
*/
void m_printAdjacent(m_graph *graph, int x) {
  int i;
  for (i = 0; i < graph->n; i++)
    if (graph->matrix[x][i] != -1)
      printf("%d ", i);

  printf("\n");
}

/** Retorna os valores dos vertices adjacentes ao vertice x
 * Parametros:
 *  - m_graph *graph: endereço do grafo
 *  - int x: vertice
 * Retorno:
 *  - int *: lista com vertices adjacentes 
 * 
*/
int *m_getAdjacent(m_graph *graph, int x) {
  // Cria mais um elemento para armazenar o tamanho utilizado do vetor
  int *vector = (int *) malloc(sizeof(int) * (graph->n) + 1);

  int i, j = 0;
  for (i = 0; i < graph->n; i++) 
    if (graph->matrix[x][i] != -1)
      vector[j++] = i;
  
  // Adiciona tamanho do vetor no ultimo campo
  vector[graph->n] = j;
  return vector;
}

/* Adiciona aresta entre os vertices x e y com peso p
  (Pesos sao sempre maiores ou iguais a 0)
  Parametros:
    - m_graph *graph: endereço do grafo
    - int x: vertice x
    - int y: vertice y
    - int p: peso da aresta
*/
void m_addAEdge(m_graph *graph, int x, int y, int p) {
  graph->matrix[x][y] = p;
}

/* Remove aresta entre os vertices x e y
  Parametros:
    - m_graph *graph: endereço do grafo
    - int x: vertice x
    - int y: vertice y
*/
void m_removeEdge(m_graph *graph, int x, int y) {
  graph->matrix[x][y] = -1;
}

/* Imprimir transposto (caso não for digrafo comando e' ignorado)
  Parametros:
    - m_graph *graph: endereço do grafo
*/
void m_printTrans(m_graph *graph) {
  int i;
  for (i = 0; i < graph->n; i++) {
    int j;
    for (j = 0; j < graph->n; j++)
      if (graph->matrix[j][i] == -1)
        printf(". ");
      else
        printf("%d ", graph->matrix[j][i]);

    printf("\n");
  }
}

/* Imprime aresta com menor peso.
  Parametros:
    - m_graph *graph: endereço do grafo
*/
void m_printWeight(m_graph *graph) {
  int i, aux = 2147483647;
  for (i = 0; i < graph->n; i++) {
    int j;
    for (j = 0; j < graph->n; j++)
      if (graph->matrix[i][j] != -1 && graph->matrix[i][j] < aux)
        aux = graph->matrix[i][j];
  }

  printf("%d\n", aux);
}

/* Imprime a aresta com os indices dos vertices em ordem crescente
  Parametros:
    - m_graph *graph: endereço do grafo
*/
void m_printWeightGraph(m_graph *graph) {
  int i, aux = 2147483647, x, y;
  for (i = 0; i < graph->n; i++) {
    int j;
    for (j = 0; j < graph->n; j++)
      if (graph->matrix[i][j] != -1 && graph->matrix[i][j] < aux) {
        aux = graph->matrix[i][j];
        x = i;
        y = j;
      }
  }

  printf("%d %d\n", x, y);
}

/** Funcao auxiliar para imprimir caminho do BFS e DFS
 * Parametros:
 *  - m_graph *graph: endereço do grafo
 *  - int pre[]: vetor de predecessores
 *  - int init: no inicial da busca
 *  - int end: no final da busca
*/
void print_path(m_graph *graph, int pre[], int init, int end) {
  int path[graph->n];
  int j = end;
  int i = 0;
  do {
    path[i++] = j;
    j = pre[j];
  } while (j != init && j != -1);

  if (j != -1) {
    path[i++] = j;
    for (j = i - 1; j >= 0; j--)
      printf("%d ", path[j]);
  }
  printf("\n");
}

/** Realiza Busca em Largura 
 * Parametros:
 *  - m_graph *graph: endereço do grafo
 *  - int init: no inicial da busca
 *  - int end: no final da busca
*/
void m_bfs(m_graph *graph, int init, int end) {
  // Verifica se precisa procurar caminho
  if (init == end) {
    printf("%d\n", init);
    return;
  }

  // Inicializando vetores auxiliares
  int color[graph->n];
  int dist[graph->n];
  int pre[graph->n];

  int i;
  for (i = 0; i < graph->n; i++) {
    color[i] = WHITE;
    dist[i] = -1;
    pre[i] = -1;
  }
  t_queue queue;
  create(&queue);
  push(&queue, init);
  dist[init] = 0;

  // Realiza busca
  while(queue != NULL) {
    i = pop(&queue);
    int *adj = m_getAdjacent(graph, i);
    
    int j;
    for (j = 0; j < adj[graph->n]; j++){
      if (color[adj[j]] == WHITE) {
        color[adj[j]] = GREY;
        dist[adj[j]] = dist[i] + 1;
        pre[adj[j]] = i;
        push(&queue, adj[j]);
      }
    }
    color[i] = BLACK;
  }

  print_path(graph, pre, init, end);
}

/** Funcao auxiliar para realizar recursao do dfs 
 * Parametros:
 *  - m_graph *graph: endereço do grafo
 *  - int u: posicao que sera percorrida
 *  - int *time: ponteiro para o tempo do dfs
 *  - int *color: ponteiro para vetor com as cores
 *  - int *pre: ponteiro para vetor com os predecessores
 *  - int *d: ponteiro para o tempo 
 *  - int *f: ponteiro para o tempo final
*/
void dfs_visit(m_graph *graph, int u, int *time, int color[], int pre[], int d[], int f[]) {
  color[u] = GREY;
  d[u] = (*time)++;

  int *adj = m_getAdjacent(graph, u);
  int j;
  for (j = 0; j < adj[graph->n]; j++) {
    if (color[adj[j]] == WHITE){
      pre[adj[j]] = u;
      dfs_visit(graph, adj[j], time, color, pre, d, f);
    }
  }

  color[u] = BLACK;
  f[u] = (*time)++;
}

/** Realiza Busca em Profundidade 
 * Parametros:
 *  - m_graph *graph: endereço do grafo
 *  - int init: no inicial da busca
 *  - int end: no final da busca
*/
void m_dfs(m_graph *graph, int init, int end) { // Verifica se precisa procurar caminho
  if (init == end) {
    printf("%d\n", init);
    return;
  }
  
  int *color = (int *) malloc(sizeof(int) * graph->n);
  int *pre = (int *) malloc(sizeof(int) * graph->n);
  int *d = (int *) malloc(sizeof(int) * graph->n);
  int *f = (int *) malloc(sizeof(int) * graph->n);

  int i;
  for (i = 0; i < graph->n; i++) {
    color[i] = WHITE;
    pre[i] = -1;
  }

  int time = 0;
  for (i = 0; i < graph->n; i++) {
    if (color[i] == WHITE)
      dfs_visit(graph, init, &time, color, pre, d, f);
    
  }

  print_path(graph, pre, init, end);
}