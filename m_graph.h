#ifndef M_GRAPH_H
#define M_GRAPH_H

#define WHITE 1
#define GREY 2
#define BLACK 3

// Matrix de Adjacencia Simples
typedef struct {
	int n;
	int **matrix;
} m_graph;

void m_create(m_graph *graph, int n);
void m_printGraph(m_graph *graph);
void m_printAdjacent(m_graph *graph, int x);
void m_addAEdge(m_graph *graph, int x, int y, int p);
void m_removeEdge(m_graph *graph, int x, int y);
void m_printTrans(m_graph *graph);
void m_printWeight(m_graph *graph);
void m_printWeightGraph(m_graph *graph);
void m_bfs(m_graph *graph, int init, int end);
void m_dfs(m_graph *graph, int init, int end);

#endif
