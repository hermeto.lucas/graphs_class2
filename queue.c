/** Codigo fonte para fila, arquivo que ira gerar o ".o"
 *
 * Lucas Hermeto - ICMC/USP 2018
*/
#include "queue.h"
#include <stdlib.h>
#include <stdio.h>

/** Cria vetor para utilizar na fila
 * Parametros:
 *  - t_queue *queue: ponteiro para fila
*/
void create(t_queue *queue) {
    *queue = NULL;
}

/** Insere um elemento na ultima posicao da fila 
 * Parametros:
 *  - t_queue *queue: ponteiro para fila
 *  - int n: valor que sera inserido
*/
void push(t_queue *queue, int n) {
    t_pointer p = (t_pointer) malloc(sizeof(t_node));
    if (p == NULL) 
        exit(EXIT_FAILURE);

    t_pointer aux = *queue;
    if (aux == NULL) {
        *queue = p;
    } else {
        while (aux->next != NULL)
            aux = aux->next;
        
        aux->next = p;
    }
    p->next = NULL;
    p->n = n;
}

/** Remove o primeiro elemento da fila
 * Parametros:
 *  - t_queue *queue: ponteiro para fila
 * Retorno:
 *  - int: Retorna valor que foi removido da fila
*/
int pop(t_queue *queue) {
    if (*queue == NULL)
        exit(EXIT_FAILURE);
    int item = (*queue)->n;
    t_pointer p = *queue;

    *queue = p->next;
    free(p);

    return item;
}
