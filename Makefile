all: main.o m_graph.o queue.o
	gcc main.o m_graph.o queue.o -o main -g -Wall -march=native -O3 -std=c99

main.o: main.c m_graph.h queue.h
	gcc -c main.c -o main.o

m_graph.o: m_graph.c m_graph.h
	gcc -c m_graph.c -o m_graph.o

queue.o: queue.c queue.h
	gcc -c queue.c -o queue.o

clean:
	rm -f *.o main

run:
	./main
