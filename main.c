/** Programa principal para realizar leitura de grafos digrafos
 * e busca de caminhos usando Busca por Profundidade ou Busca em Largura
 * 
 * Lucas Hermeto - ICMC/USP 2018
*/
#include <stdio.h>
#include <string.h>
#include "queue.h"
#include "m_graph.h"

int main(int argc, char const *argv[]) {
  m_graph graph;
  int nodes, edges;

  // Ler entrada para decidir grafo e cria-lo
  scanf("%d %d\n", &nodes, &edges);
  m_create(&graph, nodes);

  // Ler entrada para preencher grafo (digrafo) sem peso
  int i;
  for (i = 0; i < edges; i++) {
    int x, y;
    scanf("%d %d", &x, &y);
    m_addAEdge(&graph,x, y, 0);
  }

  // Comeca a ler os caminhos que devem ser gerados
  int init, end;
  while (scanf("%d %d", &init, &end) != EOF) {
    m_dfs(&graph, init, end);
  }

  return 0;
}
